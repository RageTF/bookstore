<%--
  Created by IntelliJ IDEA.
  User: Rage
  Date: 20.11.2016
  Time: 22:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <title>Checkout</title>
</head>
<body>

<%
    String login = (String) session.getAttribute("login");
    if (login == null) {
        response.sendRedirect("/autorization");
} else {
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/bucket"><img src="images/basket.png" width="25px"></a></li>
            <li><a href="/profile" class="btn btn-link"><%=login%>
            </a></li>
            <li><a href="/logout" class="btn btn-link">LogOut</a></li>
        </ul>
    </div>
</nav>

<%
    }
%>

<a style="text-decoration: none" href="home"><h1 class="logo">BookStore</h1></a>
</body>
</html>
