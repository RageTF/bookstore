<%@ page import="java.util.ArrayList" %>
<%@ page import="ru.kpfu.itis.KhanYuri.data.Book" %><%--
  Created by IntelliJ IDEA.
  User: Rage
  Date: 20.11.2016
  Time: 20:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link href="styles/main.css" rel="stylesheet">
    <link href="styles/book.css" rel="stylesheet">
    <script src="scripts/ajax.js" type="text/javascript"></script>
    <script>

        function removeFromBucket(id_book) {

            removeFromBasket(id_book)
            var rem = document.getElementById(id_book);
            rem.remove();

        }

    </script>
    <title>Bucket</title>
</head>
<body>

<%
    ArrayList<Book> bucket = (ArrayList<Book>) request.getAttribute("Bucket");
    String login = (String) session.getAttribute("login");
    if (login == null) {
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/checkout">CheckOut</a></li>
            <li><a href="/autorization" class="btn btn-link">Sign In</a></li>
            <li><a href="/registration" class="btn btn-link">Sign Up</a></li>
        </ul>
    </div>
</nav>

<%
} else {
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <
            <li><a href="/checkout">CheckOut</a></li>
            <li><a href="/profile" class="btn btn-link"><%=login%>
            </a></li>
            <li><a href="/logout" class="btn btn-link">LogOut</a></li>
        </ul>
    </div>
</nav>

<%
    }
%>

<a style="text-decoration: none" href="home"><h1 class="logo">BookStore</h1></a>


<div class="container text-center">
    <ul class="list-group">
        <%
            for (Book book : bucket) {
        %>

        <li class="list-group-item" id="<%=book.getId()%>">
            <ul class="list-inline">
                <li><a style="cursor: pointer" href="/book?id=<%=String.valueOf(book.getId())%>"><%=book.getName()%><br><%=book.getAuthor()%>
                </a></li>
                <li>
                    <button class="btn btn-primary" onclick="removeFromBucket(<%=book.getId()%>)">Remove</button>
                </li>
            </ul>

        </li>

        <%
            }
        %>

    </ul>
</div>


</body>
</html>
