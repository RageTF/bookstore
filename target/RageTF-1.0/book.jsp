<%@ page import="ru.kpfu.itis.KhanYuri.controller.Book" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="ru.kpfu.itis.KhanYuri.data.Comment" %><%--
  Created by IntelliJ IDEA.
  User: Rage
  Date: 17.11.2016
  Time: 15:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link href="styles/main.css" rel="stylesheet">
    <link href="styles/book.css" rel="stylesheet">
    <script src="scripts/ajax.js" type="text/javascript"></script>
    <title>Book</title>
</head>
<body>

<%
    String login = (String) session.getAttribute("login");
    if (login == null) {
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/bucket"><img src="images/basket.png" width="25px"></a></li>
            <li><a href="/autorization" class="btn btn-link">Sign In</a></li>
            <li><a href="/registration" class="btn btn-link">Sign Up</a></li>
        </ul>
    </div>
</nav>

<%
} else {
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/bucket"><img src="images/basket.png" width="25px"></a></li>
            <li><a href="/profile" class="btn btn-link"><%=login%>
            </a></li>
            <li><a href="/logout" class="btn btn-link">LogOut</a></li>
        </ul>
    </div>
</nav>

<%
    }
%>

<a style="text-decoration: none" href="home"><h1 class="logo">BookStore</h1></a>

<%

    ru.kpfu.itis.KhanYuri.data.Book book = (ru.kpfu.itis.KhanYuri.data.Book) request.getAttribute("book");
    String name = book.getName();
    String author = book.getAuthor();
    String description = book.getDescription();
    String pathCover = book.getPath();
    int price = book.getPrice();
    int id = book.getId();

%>

<div class="container text-center book">

    <div class="row">
        <div class="col-xs-4">
            <div>
                <img src="<%=pathCover%>"><br>
                <button class="btn btn-primary" onclick="add(<%=id%>)"><%=price%> RUB</button>
            </div>
        </div>
        <div class="col-xs-5">
            <dl class="dl-horizontal">

                <dt>Name:</dt>
                <dd><%=name%>
                </dd>
                <dt>Author:</dt>
                <dd><%=author%>
                </dd>
                <dt>Description:</dt>
                <dd><%=description%>
                </dd>

            </dl>
        </div>
    </div>
    <%
        if (login != null) {
    %>
    <div style="margin-top: 50px">

        <form action="/data" method="post">

            <label for="comment">Comment:</label>
            <textarea class="form-control" rows="5" name="comment" id="comment" style="resize: none"></textarea>
            <input type="submit" class="btn btn-link" value="Send">
            <input type="text" hidden="true" value="<%=book.getId()%>" name="book_id">


        </form>

    </div>
    <%
        }
    %>

    <div style="margin-top: 30px">

        <%
            ArrayList<Comment> comments = (ArrayList<Comment>) request.getAttribute("comments");
            if (comments != null && comments.size() != 0) {
        %>

        <ul class="list-group">

            <%
                for (Comment comment : comments) {
            %>

            <li style="list-style-type: none">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong><%=comment.getLogin()%>
                        </strong>
                    </div>
                    <div class="panel-body">
                        <%=comment.getComment()%>
                    </div>
                </div>

            </li>

            <%
                }
            %>

        </ul>
        <%
            }
        %>

    </div>

</div>


</body>
</html>
