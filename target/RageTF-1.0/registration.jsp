<%--
  Created by IntelliJ IDEA.
  User: Rage
  Date: 29.10.2016
  Time: 23:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <script src="/ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link href="styles/main.css" rel="stylesheet">
    <script src="scripts/ajax.js" type="text/javascript"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script>

        function sendFormRegistration() {

            var form = document.forms[0];

            if(checkLogin()==true){
                if(checkPassword()==true){
                    if(checkConfirm()==true){
                        if(checkEmail()==true){
                            form.submit();
                        }else{
                            alert("Incorrect E-mail!");
                        }
                    }else{
                        alert("Incorrect confirm password!");
                    }
                }else{
                    alert("Incorrect password!");
                }
            }else{
                alert("Incorrect login!");
            }

        }

        function checkLogin() {
            var login=document.getElementById("login");
            var res=login.classList.contains("alert-success");
            checkField("login",login.value)
            return res;
        }

        function checkEmail() {
            var email=document.getElementById("email");
            var res = email.classList.contains("alert-success");
            checkField("email",email.value)
            return res;
        }

        function checkPassword() {
            var passwordElement = document.getElementById("password");
            var password = passwordElement.value;

            if(password==""){
                passwordElement.classList.remove("alert-success");
                passwordElement.classList.remove("alert-danger");
                return false;
            }

            if (password.length < 20) {
                passwordElement.classList.add("alert-success");
                passwordElement.classList.remove("alert-danger");
                return true;
            } else {
                passwordElement.classList.remove("alert-success");
                passwordElement.classList.add("alert-danger");
                return false;
            }
        }

        function checkConfirm() {
            var passwordElement = document.getElementById("password");
            var password = passwordElement.value;
            var confirmElement = document.getElementById("confirm");
            var confirm = confirmElement.value;

            if(confirm==""){
                confirmElement.classList.remove("alert-success");
                confirmElement.classList.remove("alert-danger");
                return false;
            }

            if (password.length < 20 && confirm == password) {
                confirmElement.classList.add("alert-success");
                confirmElement.classList.remove("alert-danger");
                return true;
                ;
            } else {
                confirmElement.classList.remove("alert-success");
                confirmElement.classList.add("alert-danger");
                return false;
            }
        }

    </script>
</head>
<body>

<%
    String login = (String) session.getAttribute("login");
    if (login == null) {
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/autorization" class="btn btn-link">Sign In</a></li>
            <li><a href="/registration" class="btn btn-link">Sign Up</a></li>
        </ul>
    </div>
</nav>

<%
    } else {
        response.sendRedirect("/home");
    }
%>

<a style="text-decoration: none" href="home"><h1 class="logo">BookStore</h1></a>

<div class="panel center-block form-panel">

    <div class="panel-heading">
        <h3 class="panel-title text-center">
            Registration</h3>
    </div>

    <div class="panel-body text-center">

        <form role="form" class="center-block" method="post" href="/registration" id="registration">
            <div>
                <input type="text" id="login" name="login" class="form-control" oninput="checkLogin()"
                       placeholder="Login" required
                       autofocus/>
            </div>
            <div>
                <input type="password" id="password" name="password" class="form-control" oninput="checkPassword()"
                       placeholder="Password" required/>
            </div>
            <div>
                <input type="password" id="confirm" name="confirm-password" class="form-control"
                       oninput="checkConfirm()"
                       placeholder="Confirm password" required/>
            </div>
            <div>
                <input type="text" id="email" name="email" class="form-control" oninput="checkEmail()"
                       placeholder="E-mail" required/>
            </div>
            <span class="label label-default">Date of birth</span>
            <div>
                <input type="date" id="birth" name="birth" class="form-control" value="1997-01-01" max="2016-11-01"
                       min="1910-11-01" required/>
            </div>
        </form>
        <br>
        <button class="btn-link" id="submit" onclick="sendFormRegistration()">Sign Up</button>

    </div>

</div>

</body>
</html>
