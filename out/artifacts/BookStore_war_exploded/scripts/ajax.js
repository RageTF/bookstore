/**
 * Created by Rage on 15.12.2016.
 */


/**
 * Created by Rage on 14.11.2016.
 */

function createRequest() {
    var Request = false;
    if (window.XMLHttpRequest) {
        Request = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        try {
            Request = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (CatchException) {
            Request = new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    if (!Request) {
        alert("Невозможно создать XMLHttpRequest");
    }
    return Request;
}

function sendGetRequest(args, path, handler) {
    var Request = createRequest();
    if (!Request) {
        return;
    }
    Request.onreadystatechange = function () {
        if (Request.readyState == 4) {
            handler(Request);
        }
    }
    if (args.length > 0) {
        path += "?" + args;
    }
    Request.open("get", path, true);
    Request.send(null);
}

function sendPostRequest(action,data,path,handler){
    var Request=createRequest();
    var param;
    if(!Request){
        return;
    }
    Request.onreadystatechange =function () {
        if(Request.readyState==4){
            if(handler!=null) {
                handler(Request);
            }
        }
    }
    if(data!=null && action!=null){
        param="action="+action+"&book_id="+data;
        Request.open("post",path,true);
        Request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        Request.send(param);
    }
}

function addToBasket(book_id) {
    sendPostRequest("add",book_id,"/data",null);
}

function removeFromBasket(book_id) {
    sendPostRequest("remove",book_id,"/data",null)
}



function checkField(type, content) {
    var Update = function (id,resp,regExp) {
        var element=document.getElementById(id);
        if (element.value=="") {
            element.classList.remove("alert-success");
            element.classList.remove("alert-danger");
        } else {
            if (resp == "true" && regExp.test(content)) {
                element.classList.add("alert-success");
                element.classList.remove("alert-danger");
            } else {
                element.classList.add("alert-danger");
                element.classList.remove("alert-success");
            }
        }
    }
    var Handler = function (Request) {
        var regExp;
        var resp = Request.responseText;
        if (type == "login") {
            regExp=new RegExp("^([a-z]|[A-Z]|[0-9]|_|-){0,15}$");
            Update("login",resp,regExp)
        } else if (type == "email") {
            regExp=new RegExp("^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$");
            Update("email",resp,regExp);
        }
    }

    var args = "action=" + type + "&" + "data=" + content;
    sendGetRequest(args, "/data", Handler);
}