<%@ page import="ru.kpfu.itis.KhanYuri.data.Data" %>
<%@ page import="ru.kpfu.itis.KhanYuri.data.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ListIterator" %>
<%@ page import="java.util.ArrayList" %>
<%--
  Created by IntelliJ IDEA.
  User: Rage
  Date: 19.10.2016
  Time: 12:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>BookStore</title>
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link href="styles/main.css" rel="stylesheet">
    <script src="scripts/ajax.js"></script>
    <script src="scripts/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<%
    String login = (String) session.getAttribute("login");
    if (login == null) {
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/bucket"><img src="images/basket.png" width="25px"></a></li>
            <li><a href="/autorization" class="btn btn-link">Sign In</a></li>
            <li><a href="/registration" class="btn btn-link">Sign Up</a></li>
        </ul>
    </div>
</nav>

<%
} else {
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/bucket"><img src="images/basket.png" width="25px"></a></li>
            <li><a href="/profile" class="btn btn-link"><%=login%>
            </a></li>
            <li><a href="/logout" class="btn btn-link">LogOut</a></li>
        </ul>
    </div>
</nav>

<%
    }
%>

<a style="text-decoration: none" href="home"><h1 class="logo">BookStore</h1></a>

<table align="center">

    <%
        boolean open = false;
        int numPage = (Integer) request.getAttribute("page");
        int countPage = (Integer) request.getAttribute("countPage");
        int count = 0;
        List<Book> books = (ArrayList<Book>) request.getAttribute("books");
        ListIterator<Book> iterator = books.listIterator();
        while (iterator.hasNext()) {
            Book book = iterator.next();
            String name = book.getName();
            String author = book.getAuthor();
            String pathCover = book.getPath();
            int price = book.getPrice();
            int id = book.getId();

            if (count < 3) {
                count++;
                if (!open) {
                    open = true;
    %>
    <tr>
        <%
            }
        %>
        <td>
            <div class="book">
                <img width="200px" src=<%=pathCover%>><br>
                <a style="cursor: pointer" href="/book?id=<%=String.valueOf(id)%>"><%=name%><br><%=author%>
                </a><br>
                <button class="btn btn-primary" onclick="addToBasket(<%=id%>)"><%=price%> RUB</button>
            </div>
        </td>
        <%
        } else {
            open = false;
            count = 0;
            iterator.previous();
        %>
    </tr>
    <%
            }
        }
        if (open) {
    %>
    </tr>
    <%
        }
    %>

</table>

<div align="center">

    <%
        int startPage = numPage - 4;

        if (startPage < 1) {
            startPage = 1;
        }

        for (int i = startPage; i <= startPage + 8 && i <= countPage; i++) {
    %>

    <%
        if (i == numPage) {
    %>
    <a class="btn btn-primary" href="/home?page=<%=i%>"><%=i%>
    </a>
    <%
    } else {
    %>
    <a class="btn btn-default" href="/home?page=<%=i%>"><%=i%>
    </a>
    <%
            }

        }
    %>


</div>
</body>
</html>