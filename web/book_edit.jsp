<%--
  Created by IntelliJ IDEA.
  User: Rage
  Date: 21.11.2016
  Time: 22:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link href="styles/main.css" rel="stylesheet">
    <script src="scripts/ajax.js" type="text/javascript"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <title>BookEdit</title>
</head>
<body>

<%
    String login = (String) session.getAttribute("login");
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/bucket"><img src="images/basket.png" width="25px"></a></li>
            <li><a href="/profile" class="btn btn-link"><%=login%></a></li>
            <li><a href="/logout" class="btn btn-link">LogOut</a></li>
        </ul>
    </div>
</nav>

<a style="text-decoration: none" href="home"><h1 class="logo">BookStore</h1></a>

<%

    ru.kpfu.itis.KhanYuri.data.Book book = (ru.kpfu.itis.KhanYuri.data.Book) request.getAttribute("book");
    String name = book.getName();
    String author = book.getAuthor();
    String description = book.getDescription();
    String pathCover = book.getPath();
    int price = book.getPrice();
    int id = book.getId();

%>

<div class="container text-center book">

    <div class="row">
        <div class="text-center center-block col-xs-4">
            <img src="<%=pathCover%>">
        </div>
        <div class="col-xs-5 text-center">
            <form action="/book_edit" method="post" id="edit">
                <input type="hidden" value="<%=id%>" name="id" >
            <dl class="dl-horizontal">
                    <dt>Name:</dt>
                    <dd><input type="text" value="<%=name%>" class="form-control" name="name"/></dd>
                    <dt>Author:</dt>
                    <dd><input type="text" value="<%=author%>" class="form-control" name="author"/>
                    </dd>
                    <dd><input type="number" value="<%=price%>" class="form-control" name="price"/>
                    </dd>
                    <dt>Description:</dt>
                    <dd><textarea class="form-control" rows="5"
                                  name="description"><%=description%></textarea>
                    </dd>
            </dl>
            </form>
            <div>
                <input type="submit" value="Save" class="btn btn-link" form="edit">
            </div>
            <div>
                <form action="/book_edit" method="post">
                    <input type="hidden" value="<%=id%>" name="remove" >
                    <input type="submit" value="Remove" class="btn btn-link">
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
