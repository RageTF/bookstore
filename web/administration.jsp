<%@ page import="java.util.ArrayList" %>
<%@ page import="ru.kpfu.itis.KhanYuri.data.Book" %><%--
  Created by IntelliJ IDEA.
  User: Rage
  Date: 21.11.2016
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link href="styles/main.css" rel="stylesheet">
    <script src="scripts/ajax.js" type="text/javascript"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <title>Administration</title>
</head>
<body>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/bucket"><img src="images/basket.png" width="25px"></a></li>
            <li><a href="/profile" class="btn btn-link"><%=(String) session.getAttribute("login")%>
            </a></li>
            <li><a href="/logout" class="btn btn-link">LogOut</a></li>
        </ul>
    </div>
</nav>

<a style="text-decoration: none" href="home"><h1 class="logo">BookStore</h1></a>

<div>

    <h2 class="logo">Administration</h2>


</div>

<div class="panel-body text-center">

    <form role="form" class="center-block" method="post" action="/administration" enctype="multipart/form-data">
        <div>
            <input type="text" id="name" name="name" class="form-control"
                   placeholder="Name" required
                   autofocus/>
        </div>
        <div>
            <input type="text" id="author" name="author" class="form-control"
                   placeholder="Author" required/>
        </div>
        <div>
            <input type="number" id="price" name="price" class="form-control" min="1" max="100000"
                   placeholder="Price" required/>
        </div>
        <div>
            <textarea class="form-control" rows="5"
                      name="description" style="resize: none"></textarea>
        </div>

        <div>
            <input type="file" name="data">
        </div>

        <div>
            <input type="submit" class="btn-link btn" value="Save">
        </div>
    </form>

</div>

<%

    ArrayList<Book> books = (ArrayList<Book>) request.getAttribute("books");
    int numPage = (Integer) request.getAttribute("numPage");
    int countPage = (Integer) request.getAttribute("countPage");

%>

<div class="container">

    <ul class="list-group">

        <%
            for (Book book : books) {
        %>
        <ul class="list-group-item">
            <a style="cursor: pointer" href="/book_edit?id=<%=String.valueOf(book.getId())%>"><%=book.getName()%>
                <br><%=book.getAuthor()%>
            </a>

        </ul>
        <%
            }
        %>


    </ul>

</div>

<div align="center">

    <%
        int startPage = numPage - 4;

        if (startPage < 1) {
            startPage = 1;
        }

        for (int i = startPage; i <= startPage + 8 && i <= countPage; i++) {
    %>

    <%
        if (i == numPage) {
    %>
    <a class="btn btn-primary" href="/administration?page=<%=i%>"><%=i%>
    </a>
    <%
    } else {
    %>
    <a class="btn btn-default" href="/administration?page=<%=i%>"><%=i%>
    </a>
    <%
            }

        }
    %>


</div>


</body>
</html>
