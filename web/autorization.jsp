<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Autorization</title>
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link href="styles/main.css" rel="stylesheet">
    <script src="scripts/bootstrap.min.js"></script>
</head>
<body>

<%
    String login=(String)session.getAttribute("login");
    if (login==null) {
%>

<nav class="navbar navbar-default">
    <div class="collapse navbar-collapse pull-right">
        <ul class="nav navbar-nav">
            <li><a href="/autorization" class="btn btn-link">Sign In</a></li>
            <li><a href="/registration" class="btn btn-link">Sign Up</a></li>
        </ul>
    </div>
</nav>

<%
    } else {
        response.sendRedirect("/home");
    }
%>

<a style="text-decoration: none" href="home"><h1 class="logo">BookStore</h1></a>

<div class="panel center-block form-panel">

    <div class="panel-heading">
        <h3 class="panel-title text-center">
            Autorization</h3>
    </div>

    <div class="panel-body text-center">

        <form role="form" class="center-block" method="post" href="/autorization" id="autorization">
            <div>
                <input type="text" name="login" class="form-control" placeholder="Login" required
                       autofocus/>
            </div>
            <div>
                <input type="password" name="password" class="form-control" placeholder="Password" required/>
            </div>
        </form>

        <div class="checkbox text-center">
            <label>
                <input type="checkbox" name="remember" form="autorization">
                Remember me
            </label>
        </div>

        <input type="submit" class="btn-link" value="Sign In" form="autorization">

    </div>
</div>

</body>
</html>
