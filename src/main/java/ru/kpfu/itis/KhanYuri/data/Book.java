package ru.kpfu.itis.KhanYuri.data;

/**
 * Created by Rage on 09.11.2016.
 */
public class Book {

    private int id;
    private String name;
    private String author;
    private String description;
    private String path;
    private int price;

    public Book(int id,String name,String author,String description,String path, int price){
        this.id=id;
        this.name=name;
        this.author=author;
        this.description=description;
        this.path=path;
        this.price=price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getPath() {
        return path;
    }

    public int getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
