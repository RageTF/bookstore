package ru.kpfu.itis.KhanYuri.data;

/**
 * Created by Rage on 20.11.2016.
 */
public class Comment {

    private String login;
    private String comment;

    public Comment(String login,String comment){
        this.login=login;
        this.comment=comment;
    }

    public String getLogin() {
        return login;
    }

    public String getComment() {
        return comment;
    }
}
