package ru.kpfu.itis.KhanYuri.data;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Rage on 19.10.2016.
 */
public class Data extends HttpServlet {

    private static int countRowInPage = 15;
    public static String driver = "org.postgresql.Driver";
    public static String url = "jdbc:postgresql://localhost:5432/bookstore";
    public static String login = "postgres";
    public static String password = "211097";
    private static Connection connection;
    private static ResultSet resultSet;

    static {

        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, login, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("action");
        String content = req.getParameter("data");
        if ("login".equals(type)) {
            if (Data.checkLogin(content)) {
                resp.getWriter().print(true);
            } else {
                resp.getWriter().print(false);
            }
        } else if ("email".equals(type)) {
            if (Data.checkEmail(content)) {
                resp.getWriter().print(true);
            } else {
                resp.getWriter().print(false);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String id = req.getParameter("book_id");
        String action = req.getParameter("action");
        String commentText = req.getParameter("comment");

        System.out.println(req.getParameter("user"));
        System.out.print(req.getParameter("data"));

        resp.getWriter().print(false);
        if (commentText != null) {
            String login = (String) req.getSession().getAttribute("login");
            Comment comment = new Comment(login, commentText);
            addComment(comment, Integer.parseInt(id));
            resp.sendRedirect("/book?id=" + id);
        }
        if ("add".equals(action)) {
            Book book = null;
            book = getBook(Integer.parseInt(id));
            System.out.print(book.getName());
            session.setAttribute(id, book);
        } else if ("remove".equals(action)) {
            session.removeAttribute(id);
        }
    }
    /*public static void closeConnect() throws SQLException {
        if (connection != null) {
            connection.close();
        }
        if (resultSet != null) {
            resultSet.close();
        }
    }*/

    public static boolean checkEmail(String email) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT email FROM profiles WHERE email=?");
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean checkLogin(String login) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT login FROM users WHERE login=?");
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean autorization(String login, String password) throws IncorrectLoginException, IncorrectPasswordException {

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT password FROM users WHERE login=?");
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String p = resultSet.getString(1);
                if (!password.equals(p)) {
                    throw new IncorrectPasswordException();
                }
            } else {
                throw new IncorrectLoginException();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean registration(String login, String password, String email, Date date) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users (login,password) VALUES (?,?)");
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            preparedStatement.executeUpdate();

            preparedStatement = connection.prepareStatement("INSERT INTO profiles (login,email,birth) VALUES  (?,?,?)");
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, email);
            preparedStatement.setDate(3, date);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static ArrayList<Book> getBookList(int numPage) {
        ArrayList<Book> bookList = new ArrayList<Book>();

        int num = (numPage - 1) * countRowInPage;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM books ORDER BY book_id ASC LIMIT 15 OFFSET " + num);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bookList.add(new Book(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getInt(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bookList;
    }

    public static int countPage() {

        int countPage = 0;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) FROM books");
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                int countRow = resultSet.getInt(1);
                countPage = countRow / countRowInPage;
                if (countRow % countRowInPage != 0) {
                    countPage++;
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countPage;
    }

    public static void editBook(Book book) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE books SET book_name='"+book.getName()
                    +"',book_author='"+book.getAuthor()
                    +"',description='"+book.getDescription()
                    +"',price="+book.getPrice()
                    +" WHERE book_id='"+book.getId()+"'");
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addBook(Book book) {

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO books(book_name,book_author,description,price,cover_path) VALUES (?,?,?,?,?)");
            preparedStatement.setString(1, book.getName());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setString(3, book.getDescription());
            preparedStatement.setInt(4, book.getPrice());
            preparedStatement.setString(5, book.getPath());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Book getBook(int bookId) {

        Book book = null;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM books WHERE book_id=?;");
            preparedStatement.setInt(1, bookId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                book = new Book(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getInt(6));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return book;
    }

    public static ArrayList<Comment> getComments(int bookId) {

        ArrayList<Comment> comments = new ArrayList<Comment>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM comments WHERE book_id=? ORDER BY id_comment DESC;");
            preparedStatement.setInt(1, bookId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                comments.add(new Comment(resultSet.getString(4), resultSet.getString(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return comments;

    }

    public static void addComment(Comment comment, int bookId) {

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO comments(login,book_id,comment) VALUES (?,?,?);");
            preparedStatement.setString(1, comment.getLogin());
            preparedStatement.setInt(2, bookId);
            preparedStatement.setString(3, comment.getComment());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteBook(int bookId){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM books WHERE book_id=?");
            preparedStatement.setInt(1,bookId);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
