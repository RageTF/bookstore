package ru.kpfu.itis.KhanYuri.controller;

import ru.kpfu.itis.KhanYuri.data.Comment;
import ru.kpfu.itis.KhanYuri.data.Data;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Rage on 17.11.2016.
 */
public class Book extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ArrayList<Comment> comments = null;
        String book_id = req.getParameter("id");
        ru.kpfu.itis.KhanYuri.data.Book book = null;
        book = Data.getBook(Integer.parseInt(book_id));
        comments = Data.getComments(Integer.parseInt(book_id));
        req.setAttribute("comments", comments);
        req.setAttribute("book", book);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/book.jsp");
        requestDispatcher.include(req, resp);
    }
}
