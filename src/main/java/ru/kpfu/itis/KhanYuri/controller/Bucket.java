package ru.kpfu.itis.KhanYuri.controller;

import ru.kpfu.itis.KhanYuri.data.Book;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by Rage on 20.11.2016.
 */
public class Bucket extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Book> bucket=new ArrayList<Book>();
        HttpSession session=req.getSession();
        Enumeration<String> enumeration=session.getAttributeNames();

        while (enumeration.hasMoreElements()){
            String id=enumeration.nextElement();
            Object book=session.getAttribute(id);
            if(book instanceof Book){
                bucket.add((Book)book);
            }
        }
        req.setAttribute("Bucket",bucket);
        RequestDispatcher requestDispatcher=req.getRequestDispatcher("/bucket.jsp");
        requestDispatcher.include(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
