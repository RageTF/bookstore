package ru.kpfu.itis.KhanYuri.controller;

import ru.kpfu.itis.KhanYuri.data.*;
import ru.kpfu.itis.KhanYuri.data.Book;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Rage on 21.11.2016.
 */
public class BookEdit extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        String login = (String) session.getAttribute("login");
        if (login != null && "admin".equals(login)) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/book_edit.jsp");
            String id = req.getParameter("id");
            int bookId;

            if (id != null) {
                bookId = Integer.parseInt(id);
                ;
            } else {
                resp.sendRedirect("/home");
                return;
            }

            ru.kpfu.itis.KhanYuri.data.Book book = Data.getBook(bookId);
            req.setAttribute("book", book);

            requestDispatcher.include(req, resp);
        }else {
            resp.sendRedirect("/home");
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String remove = req.getParameter("remove");
        if (remove != null) {
            System.out.print("remove");
            Data.deleteBook(Integer.parseInt(remove));
            resp.sendRedirect("/administration");
        } else {
            String id = req.getParameter("id");
            int bookId = Integer.parseInt(id);
            Book book = Data.getBook(bookId);
            book.setName(req.getParameter("name"));
            book.setAuthor(req.getParameter("author"));
            book.setDescription(req.getParameter("description"));
            book.setPrice(Integer.parseInt(req.getParameter("price")));
            Data.editBook(book);
            resp.sendRedirect("/book_edit?id=" + id);
        }

    }
}
