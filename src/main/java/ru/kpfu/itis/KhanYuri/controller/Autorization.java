package ru.kpfu.itis.KhanYuri.controller;

import ru.kpfu.itis.KhanYuri.data.Data;
import ru.kpfu.itis.KhanYuri.data.IncorrectLoginException;
import ru.kpfu.itis.KhanYuri.data.IncorrectPasswordException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by Rage on 06.11.2016.
 */
public class Autorization extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher requestDispatcher;
        HttpSession session = req.getSession();
        String user = (String) session.getAttribute("login");

        if (user != null) {
            resp.sendRedirect("/home");
        } else {

            Cookie[] cookies = req.getCookies();
            Cookie userCookie;
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("user_data_bookstore")) {
                    userCookie = cookie;
                    String cookieValue = userCookie.getValue();
                    StringBuilder read = new StringBuilder();
                    String login = "";
                    String pass = "";
                    int i = 0;
                    boolean isLogin = true;

                    while (i < cookieValue.length()) {
                        if (cookieValue.charAt(i) == '[') {
                            i++;
                            while (cookieValue.charAt(i) != ']') {
                                read.append(cookieValue.charAt(i++));
                            }
                            if (isLogin) {
                                login = read.toString();
                                isLogin = false;
                            } else {
                                pass = read.toString();
                            }
                            read = new StringBuilder();
                            i++;
                        }

                        try {
                            if (Data.autorization(login, pass)) {
                                resp.sendRedirect("/home");
                                session.setAttribute("login", login);
                                session.setAttribute("password", pass);
                            } else {
                                userCookie.setMaxAge(0);
                            }
                        }catch (Exception e) {
                            userCookie.setMaxAge(0);
                        }
                    }
                }
            }
            requestDispatcher = req.getRequestDispatcher("/autorization.jsp");
            requestDispatcher.include(req, resp);
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        String login = req.getParameter("login");
        String pass = req.getParameter("password");
        String rem = req.getParameter("remember");

        try {
            if (Data.autorization(login, pass)) {

                if ("on".equals(rem)) {

                    int timeCookie = 60 * 60;

                    Cookie userCookie = new Cookie("user_data_bookstore", "[" + login + "]" + "[" + pass + "]");
                    userCookie.setMaxAge(timeCookie);
                    resp.addCookie(userCookie);

                }

                session.setAttribute("login", login);
                session.setAttribute("password", pass);
                resp.sendRedirect("/home");

            }else{
                RequestDispatcher requestDispatcher=req.getRequestDispatcher("/autorization");
                requestDispatcher.forward(req,resp);
            }
        } catch (IncorrectLoginException e) {
            System.out.print("Incorrect Login");
        } catch (IncorrectPasswordException e){
            System.out.print("Incorrect Password");
        }
    }
}
