package ru.kpfu.itis.KhanYuri.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Rage on 07.11.2016.
 */
public class Profile extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        HttpSession session=req.getSession();
        String login=(String)session.getAttribute("login");
        if("admin".equals(login)){
            requestDispatcher=req.getRequestDispatcher("/administration");
            requestDispatcher.forward(req,resp);
        }else{

            requestDispatcher=req.getRequestDispatcher("/profile.jsp");
            requestDispatcher.forward(req,resp);
        }

    }
}
