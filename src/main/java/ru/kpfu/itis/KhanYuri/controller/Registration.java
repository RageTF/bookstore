package ru.kpfu.itis.KhanYuri.controller;

import ru.kpfu.itis.KhanYuri.data.Data;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

/**
 * Created by Rage on 06.11.2016.
 */
public class Registration extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/registration.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        Date birth = Date.valueOf(req.getParameter("birth"));

        Data.registration(login, password, email, birth);

        req.getSession().setAttribute("login", login);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/autorization");
        requestDispatcher.forward(req, resp);

    }

}
