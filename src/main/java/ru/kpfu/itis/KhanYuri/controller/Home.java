package ru.kpfu.itis.KhanYuri.controller;

import ru.kpfu.itis.KhanYuri.data.Data;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Rage on 19.10.2016.
 */
public class Home extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/home.jsp");
        ArrayList<ru.kpfu.itis.KhanYuri.data.Book> books;
        int countPage;
        String page = req.getParameter("page");
        int numPage = 1;
        if (page != null) {
            numPage = Integer.parseInt(page);
            req.setAttribute("page", numPage);
        } else {
            req.setAttribute("page", numPage);
        }
        books = Data.getBookList(numPage);
        countPage = Data.countPage();
        req.setAttribute("books", books);
        req.setAttribute("countPage", countPage);
        requestDispatcher.include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int numPage = (Integer) req.getAttribute("page");

        if (req.getParameter("next") != null) {
            numPage++;
        } else if (req.getParameter("previous") != null) {
            numPage--;
        }

        req.setAttribute("page", numPage);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/home.jsp");
        requestDispatcher.include(req, resp);
    }
}
