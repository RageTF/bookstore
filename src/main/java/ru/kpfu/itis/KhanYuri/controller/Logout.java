package ru.kpfu.itis.KhanYuri.controller;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by Rage on 21.11.2016.
 */
public class Logout extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session=req.getSession();
        String login=(String)session.getAttribute("login");
        Cookie[] cookies=req.getCookies();
        for(Cookie cookie:cookies){
            if(cookie.getName().equals("user_data_bookstore")){
                cookie.setMaxAge(0);
                resp.addCookie(cookie);
            }
        }
        session.removeAttribute("login");
        resp.sendRedirect("/home");
    }
}
