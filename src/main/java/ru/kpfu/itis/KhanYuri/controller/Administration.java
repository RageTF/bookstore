package ru.kpfu.itis.KhanYuri.controller;

import ru.kpfu.itis.KhanYuri.data.Book;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import ru.kpfu.itis.KhanYuri.data.Data;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Rage on 21.11.2016.
 */
public class Administration extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session=req.getSession();
        String login =(String)session.getAttribute("login");

        if("admin".equals(login)) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/administration.jsp");
            String page = req.getParameter("page");
            ArrayList<Book> books;
            int countPage;
            int numPage;
            if (page == null) {
                numPage = 1;
            } else {
                numPage = Integer.parseInt(page);
            }

            countPage = Data.countPage();
            books = Data.getBookList(numPage);
            req.setAttribute("countPage", countPage);
            req.setAttribute("books", books);
            req.setAttribute("numPage", numPage);
            requestDispatcher.include(req, resp);
        }else{
            resp.sendRedirect("/home");
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HashMap<String, String> parameters = new HashMap<String, String>();
        String path = null;

        boolean isMultipart = ServletFileUpload.isMultipartContent(req);
        if (!isMultipart) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(1024 * 1024);
        File tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
        factory.setRepository(tempDir);
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(1024 * 1024 * 10);

        try {
            List items = upload.parseRequest(req);
            Iterator iter = items.iterator();

            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();

                if (item.isFormField()) {
                    parameters.put(item.getFieldName(), item.getString());
                } else {
                    path = processUploadedFile(item);
                    System.out.print("FILE");
                }
            }
            Book book = new Book(-1, parameters.get("name"), parameters.get("author"), parameters.get("description"), path, Integer.parseInt(parameters.get("price")));
            Data.addBook(book);

        }catch (Exception e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        resp.sendRedirect("/administration");
    }

    private String processUploadedFile(FileItem item) throws Exception {
        File uploadedFile = null;
        String p;
        Random random = new Random();
        File file=new File("/cover_books");
        if(!file.exists()){
            file.mkdirs();
        }
        do {
            p = "/cover_books/" + random.nextInt();
            String path = getServletContext().getRealPath(p);
            System.out.print(path);
            uploadedFile = new File(path);
        } while (uploadedFile.exists());
        uploadedFile.createNewFile();
        item.write(uploadedFile);
        return p;
    }
}
