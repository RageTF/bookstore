import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.kpfu.itis.KhanYuri.data.Data;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Rage on 23.11.2016.
 */


public class MainTest {

    @Test
    public void testConnectionDataBase(){
        boolean connect=false;
        Connection connection=null;
        try {
            Class.forName(Data.driver);
            connection = DriverManager.getConnection(Data.url, Data.login, Data.password);
            connect=true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            connect=false;
        } catch (SQLException e) {
            e.printStackTrace();
            connect=false;
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                connect=false;
            }
        }
        assertTrue("Есть подключение у базе данных!",connect);
    }

}
